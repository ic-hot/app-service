package ic.service.commands


import ic.base.strings.ext.orNullIfBlank
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.NotNeededException
import ic.ifaces.stoppable.Stoppable
import ic.service.Service
import ic.service.ext.startBlocking

import ic.cmd.Command
import ic.cmd.Console


abstract class RunCommand : Command(), Stoppable {


	override val name        get() = "run"
	override val syntax      get() = "run <tier>"
	override val description get() = "Run as ordinary app"


	protected abstract fun initService (args: String) : Service


	@Volatile
	private var service : Service? = null

	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun (console: Console, args: String) {

		val tierString : String? = args.orNullIfBlank

		ic.app.tier.tierString = tierString

		val service = initService(args)
		this.service = service

		console.writeLine("Starting service...")
		try {
			service.startBlocking()
			console.writeLine("Service started.")
		} catch (throwable: Throwable) {
			throwable.printStackTrace()
		} finally {
			service.waitFor()
			console.writeLine("Service stopped.")
		}

	}


	@Throws(NotNeededException::class)
	override fun stopNonBlockingOrThrowNotNeeded() {
		val service = this.service ?: throw NotNeededException
		service.stopNonBlockingOrThrowNotNeeded()
		this.service = null
	}


	override fun waitFor() {
		service?.waitFor()
	}


}