package ic.service.commands


import ic.base.strings.ext.orNullIfBlank
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.WrongStateException
import ic.service.funs.serviceName
import ic.service.funs.startRuntimeAppAsService
import ic.storage.res.funs.getResString

import ic.cmd.Command
import ic.cmd.Console


class StartCommand : Command() {


	override val name        get() = "start"
	override val syntax      get() = "start <tier>"
	override val description get() = "Start service"


	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun (console: Console, args: String) {

		console.writeLine("Starting service...")

		val tierString : String? = args.orNullIfBlank

		try {
			startRuntimeAppAsService(
				serviceName = serviceName,
				args = "run $args",
				tierString = tierString
			)
			console.writeLine(getResString("app-service/service-started"))
		} catch (wrongState: WrongStateException) {
			console.writeLine(getResString("app-service/service-already-started"))
		}

	}


}