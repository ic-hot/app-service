package ic.service.commands


import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.WrongStateException
import ic.service.funs.serviceName
import ic.service.funs.stopRuntimeAppService
import ic.storage.res.funs.getResString

import ic.cmd.Command
import ic.cmd.Console


class StopCommand : Command() {

	override val name get() = "stop"
	override val syntax get() = "stop"
	override val description get() = "Stop service"

	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun(console: Console, args: String) {
		try {
			stopRuntimeAppService(serviceName)
		} catch (wrongState: WrongStateException) {
			console.writeLine(getResString("app-service/service-not-running"))
		}
	}

}