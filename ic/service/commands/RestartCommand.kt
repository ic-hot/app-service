package ic.service.commands


import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException

import ic.cmd.Command
import ic.cmd.Console
import ic.service.funs.restartRuntimeAppService
import ic.service.funs.serviceName
import ic.storage.res.funs.getResString


class RestartCommand : Command() {

	override val name get() = "restart"
	override val syntax get() = "restart [<user_name>:<user_password>]"
	override val description get() = "Restart service"

	@Throws(InvalidSyntaxException::class, MessageException::class)

	override fun implementRun(console: Console, args: String) {
		console.writeLine(getResString("app-service/service-restarting"))
		restartRuntimeAppService(serviceName, "run $args")
		console.writeLine(getResString("app-service/service-restarted"))
	}

}