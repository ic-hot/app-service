package ic.service.commands


import ic.service.Service


internal inline fun RunCommand (

	crossinline initService : (args: String) -> Service

) : RunCommand {

	return object : RunCommand() {

		override fun initService (args: String) = initService(args)

	}

}