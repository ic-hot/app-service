package ic.service


import ic.app.CommandLineApp
import ic.service.commands.*
import ic.struct.list.List
import ic.struct.list.ext.plus

import ic.cmd.Command
import ic.cmd.SelectorCommand

import ic.dist.runtimeAppPackageName


abstract class ServiceApp : CommandLineApp() {


	protected abstract fun initService (args: String) : Service


	open fun initAdditionalCommands() : List<Command> = List()


	override fun initCommand() : Command {

		return object : SelectorCommand() {

			override val name get() = runtimeAppPackageName
			override val syntax get() = runtimeAppPackageName
			override val description get() = "A service app"
			override val shellWelcome get() = null
			override val shellTitle get() = "Service app control shell:"

			override val children = List(

				RunCommand(
					initService = ::initService
				),

				StartCommand(),

				StopCommand(),

				RestartCommand()

			) + initAdditionalCommands()

		}

	}


}