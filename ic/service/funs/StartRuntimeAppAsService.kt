package ic.service.funs


import ic.base.throwables.WrongStateException
import ic.system.startBashCommandAsService

import ic.dist.Distribution
import ic.dist.runtimeAppPackageName
import ic.text.TextBuffer


@Throws(WrongStateException::class)
internal fun startRuntimeAppAsService (

	serviceName: String,

	args : String,

	tierString : String? = ic.app.tier.tierString,

) {

	val distributionDirectory = Distribution.runtime.rootDirectory

	val command = TextBuffer().apply {
		write("IC_DISTRIBUTION_PATH=${ distributionDirectory.absolutePath } ")
		if (tierString != null) {
			write("IC_TIER=$tierString ")
		}
		write(
			distributionDirectory.absolutizePath(Distribution.runtime.launchPath) + "/"
		)
		write(
			runtimeAppPackageName + " " + args
		)
	}.toString()

	startBashCommandAsService(
		serviceName = serviceName,
		command = command
	)

}