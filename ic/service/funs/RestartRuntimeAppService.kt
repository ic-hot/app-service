package ic.service.funs


import ic.base.throwables.InconsistentDataException
import ic.base.throwables.WrongStateException


fun restartRuntimeAppService (serviceName: String, args: String) {

	try {
		stopRuntimeAppService(serviceName)
	} catch (wrongState: WrongStateException) {}

	try {
		startRuntimeAppAsService(serviceName, args)
	} catch (wrongState: WrongStateException) { throw InconsistentDataException(wrongState) }

}