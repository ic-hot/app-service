package ic.service.funs


import ic.base.throwables.WrongStateException
import ic.system.stopService


@Throws(WrongStateException::class)
fun stopRuntimeAppService (serviceName: String) {

	stopService(serviceName = serviceName)

}