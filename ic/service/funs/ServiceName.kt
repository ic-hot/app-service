package ic.service.funs


import ic.app.tier.tierString

import ic.dist.runtimeAppPackageName


internal val serviceName : String get() {

	val tierString = tierString

	if (tierString == null) {

		return runtimeAppPackageName

	} else {

		return "$runtimeAppPackageName.$tierString"

	}

}